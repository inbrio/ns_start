import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { LoginComponent } from "./components/login.component";
import { AuthenticationRoutingModule } from "./authentication.routing";

@NgModule({ 
    imports: [
        AuthenticationRoutingModule
    ],
    declarations: [
        LoginComponent
    ]
})

export class AuthenticationModule { }
