import { Component, OnInit } from "@angular/core";
import { getCssFileName, start } from "application";

import { Item } from "./item";
import { ItemService } from "./item.service";

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html",
    styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
    items: Item[];

    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class. 
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    constructor(private itemService: ItemService) {
        console.log("cest cool! 1");

        let fileName = getCssFileName();
        console.log(`fileName ${fileName}`);
    }

    ngOnInit(): void {
        this.items = this.itemService.getItems();
        console.log("cest cool! 2");
    }
}